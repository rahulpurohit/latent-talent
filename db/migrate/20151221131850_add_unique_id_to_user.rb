class AddUniqueIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :unique_id, :integer
  end
end
