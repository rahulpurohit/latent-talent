class AddReadAndConversationId < ActiveRecord::Migration
  def change
  add_column :messages, :read, :boolean

  add_column :messages, :conversation_id, :integer

  end
end
