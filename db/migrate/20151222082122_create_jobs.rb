class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :company_name
      t.string :job_title
      t.string :job_location
      t.string :salary
      t.string :experience
      t.string :sector
      t.string :skill
      t.string :level_of_experience
      t.string :corporate_type
      t.text :job_description

      t.timestamps null: false
    end
  end
end
