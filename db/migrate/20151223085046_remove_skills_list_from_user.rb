class RemoveSkillsListFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :skills_list, :string
  end
end
