class AddColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :company_name, :string
    add_column :users, :hr_name, :string
    add_column :users, :hiring_agency, :boolean
  end
end
