class AddSkillsListToUser < ActiveRecord::Migration
  def change
    add_column :users, :skills_list, :string
  end
end
