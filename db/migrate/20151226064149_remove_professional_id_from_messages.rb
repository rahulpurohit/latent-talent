class RemoveProfessionalIdFromMessages < ActiveRecord::Migration
  def change
    remove_column :messages, :professional_id, :integer
  end
end
