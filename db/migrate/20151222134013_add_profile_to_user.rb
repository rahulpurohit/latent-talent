class AddProfileToUser < ActiveRecord::Migration
  def change
    add_column :users, :sector, :string
    add_column :users, :skills, :string
    add_column :users, :level_of_experience, :string
    add_column :users, :experience, :string
    add_column :users, :corporate_type, :string
    add_column :users, :education_level, :string
    add_column :users, :major, :string
    add_column :users, :minor, :string
    add_column :users, :institution_type, :string
    add_column :users, :cerificate, :string
    add_column :users, :salary, :string
    add_column :users, :location, :string
    add_column :users, :city, :string
    add_column :users, :reloaction_status, :boolean
  end
end
