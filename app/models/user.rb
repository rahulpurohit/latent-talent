class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable



  has_many :posts
  has_many :comments
  has_many :messages
  has_many :conversations , :foreign_key => :sender_id , :class_name => "Conversation"
  has_many :conversations , :foreign_key => :rec_id , :class_name => "Conversation"

end
