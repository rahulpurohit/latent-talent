class Conversation < ActiveRecord::Base
has_many :messages , dependent: :destroy

belongs_to :sender , :foreign_key =>  :sender_id, :class_name => "User"
belongs_to :rec , :foreign_key => :rec_id, :class_name => "User"

scope :between, ->(sender_id,rec_id) do 
    where("(conversations.sender_id = ? AND conversations.rec_id = ?) OR (conversations.sender_id = ? AND conversations.rec_id = ?)",sender_id,rec_id,rec_id,sender_id)
end
	
end
