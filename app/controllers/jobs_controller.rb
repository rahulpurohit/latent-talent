class JobsController < ApplicationController
  before_action :authenticate_user!
  def index
  	@jobs = Job.all
  end
  def new
  	@job = Job.new
  end

  def create
  	@job = Job.new(job_params)
  	if @job.save
  		redirect_to jobs_path
  	else
  		root_path
  	end
  end

  def show
  	@job = Job.find(params[:id])
  end

  private
  def job_params
  	params.require(:job).permit(:company_name,
   :job_title,
   :job_location,
   :salary,
   :experience,
   :sector,
   :skill,
   :level_of_experience,
   :corporate_type,
   :job_description)
  end
end
