class WelcomesController < ApplicationController
  def index
  end

  def trend
  end

  def faq
  end

  def terms_of_serives
  end

  def privacy_policy
  end
end
