class CommentsController < ApplicationController
	before_action :authenticate_user!
	def create
		@post = Post.find(params[:post_id])
		@comment = @post.comments.create(comment_params)
		# @comment.update_attributes(user_id: current_user.id)
		 if @comment.save
		 	flash[:notice] = "Success"
		 else
		 	flash[:error] = @comment.errors.full_messages
		 end
		redirect_to post_path(@post)

	end

	def edit
		@post = Post.find(params[:post_id])
		@comment = Comment.find(params[:id])
	end

	def update
		@post = Post.find(params[:post_id])
		@comment = Comment.find(params[:id])
  			if @comment.update(comment_params)
  				flash[:notice] = "Success"
    			redirect_to @post
  			else
    			render 'edit'
  			end
	end

	def destroy
    	@post = Post.find(params[:post_id])
    	@comment = @post.comments.find(params[:id])
    	@comment.destroy
    	flash[:notice] = "Success"
    	redirect_to post_path(@post)
  	end
	private 
	def comment_params
		params.require(:comment).permit(:comment, :user_id, :post_id)
	end
end
