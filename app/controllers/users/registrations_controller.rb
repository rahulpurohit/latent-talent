class Users::RegistrationsController < Devise::RegistrationsController
	def index
		@users = User.all
	end
	def new
		@user = User.new
	end

	def create
		@user = User.new(params_user)
		logger.info "------#{@user.valid?} =======#{@user.errors.full_messages}"
		if @user.save
			redirect_to root_path
		end
	end
private
def params_user
	params.require(:user).permit(:email, :password, :phone_no, :verification_code, :unique_id, :company_name, :hr_name, :hiring_agency, :role)

end

end