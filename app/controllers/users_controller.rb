class UsersController < ApplicationController
	def show
		@user = User.find(params[:id])
	end

	def edit
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])
		@user.update_attributes(params_user)
		if @user.save
			redirect_to root_path
		end
	end
private
def params_user
	params.require(:user).permit(:email, :password, :phone_no, :verification_code, :unique_id, :company_name, :hr_name, :hiring_agency,
	:sector,
    :skills,
    :level_of_experience,
    :experience,
    :corporate_type,
    :education_level,
    :major,
    :minor,
    :institution_type,
    :cerificate,
    :salary,
    :location,
    :city,
    :reloaction_status,
    :role)
end
end
